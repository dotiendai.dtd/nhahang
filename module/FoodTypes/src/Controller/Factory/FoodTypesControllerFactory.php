<?php

namespace FoodTypes\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceManager;
use FoodTypes\Service\FoodTypeManager;
use FoodTypes\Controller\FoodTypesController;

class FoodTypesControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $foodtypeManager = $container->get(FoodTypeManager::class);
        return new FoodTypesController($entityManager, $foodtypeManager);
    }
}