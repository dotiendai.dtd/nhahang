<?php

namespace FoodTypes\Entity;
use Doctrine\ORM\Mapping;
/**
 * @Mapping\Entity
 * @Mapping\Table(name="food_type")
 */
class FoodTypes{

    //`id`, `name`, `description`, `image`

    /**
     * @Mapping\Id
     * @Mapping\Column(type="integer")
     * @Mapping\GeneratedValue
     */
    private $id;

    /** @Mapping\Column(type="string") */
    private $name;

    /** @Mapping\Column(type="string") */
    private $description;

    /** @Mapping\Column(type="string") */
    private $image;

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
    }
    
    public function getImage(){
        return $this->image;
    }
    public function setImage($image){
        $this->image = $image;
    }


}

?>