<?php

namespace Foods\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceManager;
use Foods\Service\FoodManager;
use Foods\Controller\FoodsController;

class FoodsControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $foodManager = $container->get(FoodManager::class);
        return new FoodsController($entityManager, $foodManager);
    }
}