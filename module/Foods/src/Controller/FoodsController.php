<?php 

namespace Foods\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Foods\Entity\Foods;
use Foods\Form\FoodsForm;
use FoodTypes\Service\FoodTypeManager;
use FoodTypes\Entity\FoodTypes;
use Foods\Service\FoodManager;
use Zend\File;
use Zend\Filter\File\Rename;
use Zend\Http\PhpEnvironment\Response;
use Zend\Validator\ValidatorChain;
use Zend\Validator\File\Size;
use Zend\Validator\File\MimeType;
//use Zend\View\Helper\ViewModel;


class FoodsController extends AbstractActionController{
    private $entityManager;
    private $foodManager;
    public function __construct($entityManager, $foodManager)
    {
        $this->entityManager = $entityManager;
        $this->foodManager = $foodManager;
    }
    public function indexAction(){
       // $foods = $this->entityManager->getRepository(Foods::class)->findAll();
       $en = $this->entityManager;
       $qb = $en->createQueryBuilder();
        $query = $qb->select('f')->from(Foods::class,'f')->orderBy('f.id_type');
        $foods = $query->getQuery()->getResult();
        // var_dump($foods->getQuery()->getSql()); die;
        // var_dump($foods); die;
       return new ViewModel(['foods'=>$foods]);
    }

    public function addAction(){
        $qb = $this->entityManager->createQueryBuilder();
        // $query = $qb->select('t','f')->from(Foods::class,'f')
        // ->innerJoin(FoodTypes::class,'t',\Doctrine\ORM\Query\Expr\Join::WITH,'f.id_type = t.id');
        $query = $qb->select('t')->from(FoodTypes::class,'t');
        $types = $query->getQuery()->getResult();
        //var_dump($query->getQuery()->getSql()); die;
       // print_r($types); die;
        $listType = [];
        foreach($types as $type){
            $idType = $type->getId();
            $nameType = $type->getName();
            $listType[$idType] = $nameType;
           // print_r($idType); 
        }
      //  print_r($listType);
        $form = new FoodsForm('add');
        $form->get('id_type')->setValueOptions($listType);

        if($this->getRequest()->isPost()){
            $data = $this->params()->fromPost();
            $file = $this->getRequest()->getFiles()->toArray();
            $data = array_merge_recursive($data, $file);

            $form->setData($data);
            if($form->isValid()){
                $data = $form->getData();
                $newName = date('Y-m-d-h-i-s').'-'.$file['image']['name'];

                $image = new Rename([
                    'target'=>IMAGE_PATH.'/hinh_mon_an/'.$newName,
                    'overwrite'=>true
                ]);
        
                $image->filter($file['image']);
                $data['update_at'] = date('Y-m-d');
                $data['image'] = $newName;
             //  var_dump($data); die;
                $this->foodManager->addFood($data);
                $this->flashMessenger()->addSuccessMessage('Thêm thành công');
                return $this->redirect()->toRoute('foods',['action'=>'index']);
                
            }
        }
        
        return new ViewModel(['form'=>$form]);
    }

    public function editAction(){
        $id = (int)$this->params()->fromRoute('id',0);
        if($id <= 0){
           $this->getResponse()->setStatusCode(404);
           return;       
        }
        $food = $this->entityManager->getRepository(Foods::class)->findOneById(['id'=>$id]);
        if(!$food){
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $qb = $this->entityManager->createQueryBuilder();
        $query = $qb->select('t')->from(FoodTypes::class,'t');
        $types = $query->getQuery()->getResult();
        $listType = [];
        foreach($types as $type){
            $idType = $type->getId();
            $nameType = $type->getName();
            $listType[$idType] = $nameType;
        }
        $form = new FoodsForm('edit');
        $form->get('id_type')->setValueOptions($listType);
        if(!$this->getRequest()->isPost()){
           $data = [
               'name'=>$food->getName(),
               'id_type'=>$food->getIdType(),
               'summary'=>$food->getSummary(),
               'detail'=>$food->getDetail(),
               'price'=>$food->getPrice(),
               'promotion'=>$food->getPromotion(),
                'image'=>$food->getImage(),
                'unit'=>$food->getUnit(),
                'today'=>$food->getToday()
           ];
          $form->setData($data);
          return new ViewModel(['form'=>$form]);
        }
        $data = $this->params()->fromPost();
        $file = $this->getRequest()->getFiles()->toArray();
        if($file['image']['error'] <= 0){
            $validatorChain = new ValidatorChain();
            $size = new Size(['min'=>20*1024,'max'=>2*1024*1024]);
            $size->setMessages([
                Size::TOO_SMALL => 'File quá nhỏ, ít nhất %min% kb',
                Size::TOO_BIG=>'Dung lượng quá lớn, tối đa %max% kb'
            ]);
            $mimeType = new MimeType('image/jpg, image/png, image/jpeg');
            $mimeType->setMessages([
                MimeType::FALSE_TYPE=>'Kiểu file %type% không được phép chọn',
                MimeType::NOT_DETECTED=>'Không xác định',
                MimeType::NOT_READABLE=>'Không thể đọc file'
            ]);
            $validatorChain->attach($mimeType,true,2)->attach($size,true,1);
            if(!$validatorChain->isValid($file['image'])){
                $message = $validatorChain->getMessages();
                $form->get('image')->setMessages($message);
                return new ViewModel(['form'=>$form]);
            }
            $data = array_merge_recursive($data,$file);
            $newName = date('Y-m-d-h-i-s').'-'.$file['image']['name'];

            $image = new Rename([
                'target'=>IMAGE_PATH.'/hinh_mon_an/'.$newName,
                'overwrite'=>true
            ]);
            $image->filter($file['image']);
            $data['image'] = $newName;
            $data['update_at'] = date('Y-m-d');
        }else{
            $data['image'] = $food->getImage();
            $data['update_at'] = date('Y-m-d');
        }
        $form->setData($data);
        if($form->isValid()){
            $this->foodManager->editFood($food, $data);
            $this->flashMessenger()->addSuccessMessage("Cập nhật thành công");
            return $this->redirect()->toRoute('foods',['action'=>'index']);
        }
        return new ViewModel(['form'=>$form]);
    }

    public function deleteAction(){
        $id = $this->params()->fromRoute('id',0);
        if($id <=0){
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $food = $this->entityManager->getRepository(Foods::class)->findOneById(['id'=>$id]);
        if(!$food){
            $this->getResponse()->setStatusCode(404);
            return;
        }
        //print_r($food->getName()); die;
        if($this->getRequest()->isPost()){
            $btnValue = $this->getRequest()->getPost('delete','No');
            if($btnValue == "Yes"){
                $this->foodManager->deleteFood($food);
                $this->flashMessenger()->addSuccessMessage('Xóa món ăn thành công');
               
            }
            return $this->redirect()->toRoute('foods',['action'=>'index']);
        }
        return new ViewModel(['food'=>$food]);
    }
}
?>