<?php

namespace Foods\Entity;
use Doctrine\ORM\Mapping;
/**
 * @Mapping\Entity
 * @Mapping\Table(name="foods")
 */
class Foods{

    //`id`, `id_type`, `name`, 
    //`summary`, `detail`, `price`, `promotion`, `image`, `update_at`, `unit`, `today`

    /**
     * @Mapping\Id
     * @Mapping\Column(type="integer")
     * @Mapping\GeneratedValue
     */
    private $id;
    
    /** @Mapping\Column(type="integer", name="id_type", unique=TRUE) */
    private $id_type;

    /** @Mapping\Column(type="string") */
    private $name;

    /** @Mapping\Column(type="string") */
    private $summary;

    /** @Mapping\Column(type="string") */
    private $detail;

    /** @Mapping\Column(type="float") */
    private $price;

    /** @Mapping\Column(type="string") */
    private $promotion;

    /** @Mapping\Column(type="string") */
    private $image;

    /** @Mapping\Column(type="datetime") */
    private $update_at;

    /** @Mapping\Column(type="string") */
    private $unit;

    /** @Mapping\Column(type="smallint") */
    private $today;

        //`id`, `id_type`, `name`, 
    //`summary`, `detail`, `price`, `promotion`, `image`, `update_at`, `unit`, `today`

    public function getUpdateAt(){
        return $this->update_at;
    }
    public function setUpdateAt($update_at){
        $this->update_at = $update_at;
    }

    public function getUnit(){
        return $this->unit;
    }
    public function setUnit($unit){
        $this->unit = $unit;
    }

    public function getToday(){
        return $this->today;
    }
    public function setToday($today){
        $this->today = $today;
    }

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getIdType(){
        return $this->id_type;
    }
    public function setIdType($id_type){
        $this->id_type = $id_type;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getSummary(){
        return $this->summary;
    }
    public function setSummary($summary){
        $this->summary = $summary;
    }

    public function getDetail(){
        return $this->detail;
    }
    public function setDetail($detail){
        $this->detail = $detail;
    }

    public function getPrice(){
        return $this->price;
    }
    public function setPrice($price){
        $this->price = $price;
    }

    public function getPromotion(){
        return $this->promotion;
    }
    public function setPromotion($promotion){
        $this->promotion = $promotion;
    }

    public function getImage(){
        return $this->image;
    }
    public function setImage($image){
        $this->image = $image;
    }
}