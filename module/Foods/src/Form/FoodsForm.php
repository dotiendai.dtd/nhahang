<?php

namespace Foods\Form;
use Zend\Form\Form;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Zend\Validator\NotEmpty;
use Zend\Validator\Digits;
use Zend\Validator\File\MimeType;
use Zend\Validator\StringLength;
use Zend\Validator\File\Size;

class FoodsForm extends Form{
    private $flag;
    public function __construct($flag = "add")
    {
        parent::__construct();
        $this->setAttributes([
            'class'=>'form-horizontal',
            'name'=>'foods-form',
            'enctype'=>'multipart/form-data'
        ]);

        $this->addElement();
        $this->addValidator();

    }

    public function addElement(){
        //id hidden auto increment
        $id = new Element\Text('id');
        $id->setAttributes([
            'type'=>'hidden',
            'id'=>'id',
            'class'=>'form-control'
        ]);
        $this->add($id);

        //name
        $name = new Element\Text('name');
        $name->setLabel('Tên món ăn: ')->setLabelAttributes([
            'for'=>'name',
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'name',
            'class'=>'form-control'
        ]);
        $this->add($name);

        //id type
        $id_type = new Element\Select('id_type');
        $id_type->setLabel('Chọn loại món ăn: ')->setLabelAttributes([
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'id_type',
            'class'=>'form-control'
        ]);
        $this->add($id_type);

        //summary
        $summary = new Element\Textarea('summary');
        $summary->setLabel('Giới thiệu ngắn: ')->setLabelAttributes([
            'for'=>'summary',
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'summary',
            'class'=>'form-control',
            'row'=>3,
            'style'=>'resize: none'
        ]);
        $this->add($summary);

        //detail
        $detail = new Element\Textarea('detail');
        $detail->setLabel('Chi tiết món ăn: ')->setLabelAttributes([
            'for'=>'detail',
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'detail',
            'class'=>'form-control'
        ]);
        $this->add($detail);

        //price
        $price = new Element\Text('price');
        $price->setLabel('Đơn giá: ')->setLabelAttributes([
            'for'=>'price',

        ])->setAttributes([
            'id'=>'price',
            'class'=>'form-control',
            'placeholder'=>'Nhập giá'
        ]);
        $this->add($price);

        //promotion
        $promotion = new Element\Select('promotion');
        $promotion->setLabel('Chọn khuyến mãi: ')->setAttributes([
            'id'=>'promotion',
            'class'=>'form-control'
        ]);
        $promotion->setValueOptions([
            'pepsi'=>'Nước ngọt',
            'tea'=>'Trà đá',
            'water'=>'Nước suối',
            'towel'=>'Khăn lạnh'
        ]);
        $this->add($promotion);

         //image
         $image = new Element\File('image');
         $image->setLabel('Choose image: ')->setLabelAttributes([
             'for'=>'image'
         ])->setAttributes([
             'multiple'=>false,
         ]);
  
         if($this->flag == "add"){
             $image->setAttributes(['required'=>'required']);
         }
         $this->add($image);

         //unit
         $unit = new Element\Text('unit');
         $unit->setLabel('Đơn vị tính: ')
         ->setLabelAttributes([
             'for'=>'unit'
         ])->setAttributes([
             'class'=>'form-control',
             'id'=>'unit',
             'placeholder'=>'Nhập đơn vị tính'
         ]);
         $this->add($unit);

         //today
         $today = new Element\Radio('today');
         $today->setLabel('Món ăn này: ')->setLabelAttributes([
             'class'=>'label-control'
         ])->setAttributes([
             'id'=>'today',
             'value'=>'0',
             'style'=>'margin-left: 20px'
         ]);
         $today->setValueOptions([
             '0'=>' Hôm nay chưa có',
             '1'=>' Hôm nay có'
         ]);
         $this->add($today);

         //button submit
         $this->add([
            'name'=>'btnSubmit',
            'type'=>Element\Submit::class,
            'attributes'=>[
                'id'=>'btnSubmit',
                'class'=>'btn btn-success',
                'value'=>'Save'
            ],
        ]);
    }

    private function addValidator(){
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'=>'name',
            'required'=>true,
            'filter'=>[
                'name'=>'StringTrim'
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY =>'Vui lòng nhập tên món',
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'max'=>150,
                        'messages'=>[
                            StringLength::TOO_LONG =>'Tên món ăn không được quá %max% kí tự',
                        ]
                    ]
                ],

            ]
        ]);
        $inputFilter->add([
            'name'=>'price',
            'required'=>true,
            'filter'=>[
                'name'=>'StringTrim'
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'break_chain_on_failure'=>true,
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY =>'Vui lòng nhập đơn giá',
                        ]
                    ]
                ],
                [
                    'name'=>'digits',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            Digits::NOT_DIGITS => 'Vui lòng nhập số'
                        ]
                    ]
                ]

            ]
        ]);

        if($this->flag == "add"){
            $inputFilter->add([
                'name'=>'image',
                'required'=>true,
                'filter'=>[
                    ['name'=>'StringTrim'],
                    ['name'=>'StripTags']
                ],
                'validators'=>[
                    [
                        'name'=>'NotEmpty',
                        'break_chain_on_failure'=>true,
                        'options'=>[
                            'break_chain_on_failure'=>true,
                            'messages'=>[
                                NotEmpty::IS_EMPTY =>'Vui lòng chọn ảnh',
                            ]
                        ]
                    ],
                    [
                        'name'=>'fileMimeType',
                        'options'=>[
                            'mimeType'=>'image/jpg, image/png, image/jpeg',
                            'break_chain_on_failure'=>true,
                            'messages'=>[
                                MimeType::FALSE_TYPE=>'Kiểu file %type% không được phép chọn',
                                MimeType::NOT_DETECTED=>'Không xác định',
                                MimeType::NOT_READABLE=>'Không thể đọc file'
                            ]
                        ]
                    ],
                    [
                        'name'=>'fileSize',
                        'options'=>[
                            'break_chain_on_failure'=>true,
                            'min'=>20*1024,
                            'max'=>2*1024*1024,
                            'messages'=>[
                                Size::TOO_SMALL => 'File quá nhỏ, ít nhất %min% kb',
                                Size::TOO_BIG=>'Dung lượng quá lớn, tối đa %max% kb'
                            ]
                        ]
                    ],
    
                ]
            ]);
        }
    } 
}