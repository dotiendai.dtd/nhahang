<?php

namespace Foods\Service;

use Foods\Service\Factory\FoodManagerFactory;
use Foods\Entity\Foods;

class FoodManager{

    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addFood($data){
        // Kiem tra xem mon an da ton tai chua, nhung vi mon an tu tang id nen k can kt
       // $qb = $this->entityManager->createQueryBuilder();
        // $query = $qb->select('f')->from(Foods::class,'f')
        // ->where($qb->expr()->orX($qb->expr()->eq('f.id', $data['id'])));
        // $checkFoods = $query->getQuery()->getResult();
        // if(!$checkFoods){
        //     return false;
        // }

        $food = new Foods();
        $food->setName($data['name']);
        $food->setPrice($data['price']);
        $food->setIdType($data['id_type']);
        $food->setSummary($data['summary']);
        $food->setDetail($data['detail']);
        $food->setPromotion($data['promotion']);
        $food->setImage($data['image']);
        $food->setUnit($data['unit']);
        $formatDate = new \DateTime($data['update_at']);
        $formatDate->format('Y-m-d');
        $food->setUpdateAt($formatDate);
        $food->setToday($data['today']);

        $this->entityManager->persist($food);
        $this->entityManager->flush();
        return $food;


    }

    public function editFood($food, $data){

        $food->setName($data['name']);
        $food->setPrice($data['price']);
        $food->setIdType($data['id_type']);
        $food->setSummary($data['summary']);
        $food->setDetail($data['detail']);
        $food->setPromotion($data['promotion']);
        $food->setImage($data['image']);
        $food->setUnit($data['unit']);
        $formatDate = new \DateTime(date('Y-m-d'));
        $formatDate->format('Y-m-d');
        $food->setUpdateAt($formatDate);
        $food->setToday($data['today']);

        $this->entityManager->flush();
        return $food;
    }

    public function deleteFood($food){
        $this->entityManager->remove($food);
        $this->entityManager->flush();
    }
}