<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Users\Form\LoginForm;
use Zend\Authentication\Result;


class AuthController extends AbstractActionController{
    
    private $entityManager;
    private $userManager;
    private $authManager;
    private $authService;

    public function __construct($entityManager, $userManager, $authManager, $authService)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->authManager = $authManager;
        $this->authService = $authService;
    }

    public function LoginAction(){
       $form = new LoginForm();
       if($this->getRequest()->isPost()){
           $data = $this->params()->fromPost();
           $form->setData($data);
           if($form->isValid()){
               $data = $form->getData();
               $result = $this->authManager->login($data['username'],$data['password'],$data['rememberMe']);
               if($result->getCode() == Result::SUCCESS){
                   //$this->flashMessenger()->addSuccessMessage('Thanh cong');
                    return $this->redirect()->toRoute('user');
               }else{
                   $messageError = current($result->getMessages()); // echo $a = current(array[])
                  $this->flashMessenger()->addErrorMessage($messageError);
                  return $this->redirect()->toRoute('auth');
               }
           }
       }
       return new ViewModel(['form'=>$form]);
    }

    public function LogoutAction(){
        $this->authManager->logout();
        return $this->redirect()->toRoute('auth',['action'=>'Login']);
    }
}