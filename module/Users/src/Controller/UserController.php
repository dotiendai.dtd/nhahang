<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Users\Entity\Users;
use Users\Form\UserForm;
use Users\Service\UserManager;
use Users\Form\ChangePassword;
use Users\Form\ResetPasswordForm;
use PHPUnit\Framework\Exception;


class UserController extends AbstractActionController{

    private $entityManager;
    private $userManager;
    public function __construct($entityManager, $userManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
    }
    public function indexAction(){
        $users = $this->entityManager->getRepository(Users::class)->findAll();
        return new ViewModel(['users'=>$users]);
    }

    public function addAction(){
        $form = new UserForm('add');
        //print_r($form); die;
        $checkRequest = $this->getRequest();
        if($checkRequest->isPost()){
            $data = $this->params()->fromPost();
            $form->setData($data);
           // print_r($form); die;
            if($form->isValid()){
                $data = $form->getData();
                $user = $this->userManager->addUser($data);
               // print_r($user);
               $this->flashMessenger()->addSuccessMessage('Thêm thành công');
               return $this->redirect()->toRoute('user',['action'=>'index']);
            }
        }
        return new ViewModel(['form'=>$form]);
    }
    public function editAction(){
        $idUser = $this->params()->fromRoute('id',0);
        if($idUser<=0){
            $this->getResponse()->getStatusCode('404');
            return;
        }
        $user = $this->entityManager->getRepository(Users::class)->find($idUser);
        //print_r($user); die;
        if(!$user){
            $this->getResponse()->getStatusCode('404');
            return;
        }
        $form = new UserForm("edit");
        $checkRequest = $this->getRequest();
        if(!$checkRequest->isPost()){
            $data = [
                'username'=>$user->getUsername(),
                'fullname'=>$user->getFullname(),
                'email'=>$user->getEmail(),
                'gender'=>$user->getGender(),
                'address'=>$user->getAddress(),
                'birthday'=>$user->getBirthdate(),
                'phone'=>$user->getPhone(),
                'role'=>$user->getRole()
            ];
         //   print_r($data); die;
            $form->setData($data);
            return new ViewModel(['form'=>$form,'user'=>$user]);
        }

        $data = $this->params()->fromPost();
       
        $form->setData($data);
        if($form->isValid()){
            $data = $form->getData();
           // print_r($data); die;
           $this->userManager->editUser($user, $data);
           $this->flashMessenger()->addSuccessMessage('update thành công');
           return $this->redirect()->toRoute('user',['action'=>'index']);
        }

        return new ViewModel(['form'=>$form,'user'=>$user]);
    }

    public function deleteAction(){
        $idUser = $this->params()->fromRoute('id',0);
        if($idUser<=0){
            $this->getResponse()->getStatusCode('404');
            return;
        }
        $user = $this->entityManager->getRepository(Users::class)->find($idUser);
        //print_r($user); die;
        if(!$user){
            $this->getResponse()->getStatusCode('404');
            return;
        }

        if($this->getRequest()->isPost()){
            $btn = $this->getRequest()->getPost('delete','No');
            if($btn =="Yes"){
                $this->userManager->deleteUser($user);
                $this->flashMessenger()->addSuccessMessage('Xóa thành công');
            }
            return $this->redirect()->toRoute('user',['action'=>'index']);
        }
        return new ViewModel(['user'=>$user]);
    }

    public function changePasswordAction(){
        $idUser = $this->params()->fromRoute('id',0);
        if($idUser<=0){
            $this->getResponse()->getStatusCode('404');
            return;
        }
        $user = $this->entityManager->getRepository(Users::class)->find($idUser);
       // print_r($user); die;
        if(!$user){
            $this->getResponse()->getStatusCode('404');
            return;
        }
        $form = new ChangePassword();
        if($this->getRequest()->isPost()){
            $data = $this->params()->fromPost();
            $form->setData($data);
            if($form->isValid()){
                $data = $form->getData();
                $checkPw = $this->userManager->changeNewPassword($user, $data);
               // var_dump($checkPw);
                if(!$checkPw){
                    $this->flashMessenger()->addErrorMessage('Mật khẩu cũ chưa đúng');
                    return $this->redirect()->toRoute('user',['action'=>'changePassword','id'=>$user->getId()]);
                }else{
                    $this->flashMessenger()->addSuccessMessage('Đổi mật khẩu thành công');
                    return $this->redirect()->toRoute('user',['action'=>'index']);
                }
            }   
        }
        //print_r($form); die;
        //$data = [
        //'password'=>$user->getPassword(), // Mật khẩu đã bị mã hóa
        //];
        //$form->setData($data);
         return new ViewModel(['form'=>$form]);
    }

    public function resetPasswordAction(){

        $form = new ResetPasswordForm();
        if($this->getRequest()->isPost()){
            $data = $this->params()->fromPost();
            $form->setData($data);
            if($form->isValid()){
                $data = $form->getData();
                $user = $this->entityManager->getRepository(Users::class)->findOneByEmail($data['email']);
                if($user !=null){
                 //   print_r($user->getEmail()); die;
                    $this->userManager->createTokenToResetPw($user);
                    $this->flashMessenger()->addSuccessMessage('Kiểm tra Mail của bạn để reset password');
                }else{
                    $this->flashMessenger()->addErrorMessage('Email không tồn tại');
                }
                return $this->redirect()->toRoute('resetpassword');
            }
        }
        return new ViewModel(['form'=>$form]);
    }

    public function setPasswordAction(){
        $token = $this->params()->fromRoute('token',null);
        if($token == null || strlen($token) != 32){
            throw new Exception("Token không hợp lệ");
        }
        if(!$this->userManager->checkResetPasswordToken($token)){
            throw new Exception("Token không hợp lệ hoặc đã hết hạn.");
        }
        $form = new ChangePassword('resetPw');
        if($this->getRequest()->isPost()){
            $data = $this->params()->fromPost();
            $form->setData($data);
            if($form->isValid()){
                $data = $form->getData();
                $newPassword = $data['newPwd'];
                if($this->userManager->setNewPasswordByToken($token, $newPassword)){
                    $this->flashMessenger()->addSuccessMessage('Đổi mật khẩu thành công');
                    return $this->redirect()->toRoute('user',['action'=>'index']);
                }else{
                    $this->flashMessenger()->addErrorMessage('Không thành công');
                    return $this->redirect()->toRoute('setpassword',['token'=>$token]);
                }
            }
        }
        return new ViewModel(['form'=>$form]);
    }
}