<?php

namespace Users\Form;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;
use Zend\Validator\Regex;
use Zend\Validator\Identical;

class ChangePassword extends Form{
    private $flag;
    public function __construct($flag = "changePw"){
        parent::__construct();
        $this->flag = $flag;
        $this->setAttributes([
            'class'=>'form-horizontal',
            'name'=>'change-password'
        ]);
            $this->addElement();
            $this->addValidator();
    }
    public function addElement(){

          //csrf
          $csrf = new \Zend\Form\Element\Csrf('csrf');
          $csrf->setAttributes([
              'id'=>'csrf',
              'class'=>'form-control',
          ])->setOptions([
              'csrf_options'=>[
                  'timeout'=>600, //10 phut
              ],
          ]);
          $this->add($csrf);

            if($this->flag == "changePw"){
                    //old password
                    $oldPw = new Element\Password('oldPwd');
                    $oldPw->setLabel('Old Password: ')->setLabelAttributes([
                        'for'=>'oldPw'
                    ])->setAttributes([
                        'id'=>'oldPw',
                        'class'=>'form-control',
                        'placeholder'=>'Nhập password'
                    ]);
                    $this->add($oldPw);
            }

        //new password
        $newPw = new Element\Password('newPwd');
        $newPw->setLabel('Mật khẩu mới: ')->setLabelAttributes([
            'for'=>'newPw',
        ]);
        $newPw->setAttributes([
            'id'=>'newPw',
            'class'=>'form-control',
            'placeholder'=>'Nhập mật khẩu mới'
        ]);
        $this->add($newPw);

        //confirm new password
        $confirm_newPw = new Element\Password('confirm_newPwd');
        $confirm_newPw->setLabel('Nhập lại mật khẩu mới: ')->setLabelAttributes([
            'for'=>'confirm_newPw',
        ]);
        $confirm_newPw->setAttributes([
            'id'=>'confirm_newPw',
            'class'=>'form-control',
            'placeholder'=>'Nhập lại mật khẩu mới'
        ]);
        $this->add($confirm_newPw);
      

        $this->add([
            'name'=>'btnSubmit',
            'type'=>\Zend\Form\Element\Submit::class,
            'attributes'=>[
                'id'=>'btnSubmit',
                'class'=>'btn btn-success',
                'value'=>'Change Password'
            ],
        ]);
    }

    public function addValidator(){
        $inputfilter = new InputFilter();
        $this->setInputFilter($inputfilter);

        if($this->flag == "changePw"){
               //old password
        $inputfilter->add([
            'name'=>'oldPwd',
            'required'=>true,
            'filter'=>[
                'name'=>'StringTrim',
                'name'=>'StripTags',
                'name'=>'StripNewLines'
            ],  
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Mật khẩu cũ không được bỏ trống'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>8,
                        'max'=>20,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[a-z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[A-Z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/\d/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
            ]
        ]);
        }

        //new password
        $inputfilter->add([
            'name'=>'newPwd',
            'required'=>true,
            'filter'=>[
                'name'=>'StringTrim',
                'name'=>'StripTags',
                'name'=>'StripNewLines'
            ],  
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Mật khẩu cũ không được bỏ trống'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>8,
                        'max'=>20,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[a-z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[A-Z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/\d/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
            ]
        ]);

        //confirm new password
        $inputfilter->add([
            'name'=>'confirm_newPwd',
            'required'=>true,
            'filter'=>[
                'name'=>'StringTrim',
                'name'=>'StripTags',
                'name'=>'StripNewLines'
            ],  
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Mật khẩu cũ không được bỏ trống'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>8,
                        'max'=>20,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[a-z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[A-Z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/\d/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Identical',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'token'=>'newPwd',
                        'messages'=>[
                            Identical::NOT_SAME=>'Password không giống nhau',
                            Identical::MISSING_TOKEN=>'Lỗi không xác định'
                        ]
                    ],
                ],
            ]
        ]);
    }
}
?>