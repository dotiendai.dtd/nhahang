<?php

namespace Users\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;
use Zend\Validator\Regex;

class LoginForm extends Form{
    public function __construct()
    {
        parent::__construct();
        $this->setAttributes([
            'class'=>'form-horizontal',
            'name'=>'form-login'
        ]);

        $this->addElement();
        $this->addValidator();

    }

    public function addElement(){
        $username = new Element\Text('username');
        $username->setLabel('Tên đăng nhập: ')->setLabelAttributes([
            'for'=>'username',
            
        ])->setAttributes([
            'id'=>'username',
            'class'=>'form-control',
            'placeholder'=>'Nhập tên đăng nhập'
        ]);
        $this->add($username);

        $password = new Element\Password('password');
        $password->setLabel('Mật khẩu: ')->setLabelAttributes(['for'=>'password'])
        ->setAttributes([
            'id'=>'password',
            'class'=>'form-control',
            'placeholder'=>'Nhập mật khẩu'
        ]);
        $this->add($password);

        $rememberMe = new Element\Checkbox('rememberMe');
        $rememberMe->setLabel('Remember me')
        ->setLabelAttributes([
            'for'=>'rememberMe'
        ])->setAttributes([
            'id'=>'rememberMe',
            'value'=>1,
            'required'=>false
        ]);
        $this->add($rememberMe);


        $this->add([
            'name'=>'btnSubmit',
            'type'=>Element\Submit::class,
            'attributes'=>[
                'id'=>'btnSubmit',
                'class'=>'btn btn-success',
                'value'=>'Login'
            ],
        ]);
    }

    public function addValidator(){
        $inputfilter = new InputFilter();
        $this->setInputFilter($inputfilter);
        $inputfilter->add([
            'name'=>'password',
            'required'=>true,
            'filter'=>[
                ['name'=>'StringTrim'],
                ['name'=>'StripTags'],
                ['name'=>'StripNewLines']
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Password không được rỗng'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>8,
                        'max'=>20,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
                        ]
                    ],
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[a-z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[A-Z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/\d/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
            ],
        ]);

        $inputfilter->add([
            'name'=>'username',
            'required'=>true,
            'filter'=>[
                'name'=>'StringTrim',
                'name'=>'StripTags',
                'name'=>'StripNewLines'
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Tên đăng nhập không được bỏ trống'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>6,
                        'max'=>18,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Username quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Username quá dài, tối đa %max% kí tự',
                        ]
                    ],
                ],
            ]
        ]);

        $inputfilter->add([
            'name'=>'rememberMe',
            'requrired'=>false,
            'validators'=>[
               [
                'name'=>'InArray',
                'options'=>[
                    'haystack'=>[0,1],
                    'messages'=>[
                        \Zend\Validator\InArray::NOT_IN_ARRAY=>'Dữ liệu không hợp lệ'
                    ]
                ]
               ]
            ]
        ]);
    }

}