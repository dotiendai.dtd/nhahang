<?php

namespace Users\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;
use Zend\Validator\Regex;
use Zend\Validator\Identical;
use Zend\Validator\EmailAddress;
use Zend\Captcha;

class ResetPasswordForm extends Form{
    public function __construct()
    {
        parent::__construct();
        $this->setAttributes([
            'class'=>'form-horizontal',
            'name'=>'reset-password'
        ]);
        $this->addElement();
        $this->addValidator();
    }

    public function addElement(){
        $email = new Element\Email('email');
        $email->setLabel('Email của bạn: ')->setLabelAttributes([
            'for'=>'email'
        ])->setAttributes([
            'id'=>'email',
            'class'=>'form-control',
            'placeholder'=>'Nhập email của bạn'
        ]);
        $this->add($email);

        // $newPw = new Element\Password('newPw');
        // $newPw->setLabel('Nhập mật khẩu mới: ')->setLabelAttributes(['for'=>'newPw'])
        // ->setAttributes([
        //     'id'=>'newPw',
        //     'class'=>'form-control',
        //     'placeholder'=>'Nhập mật khẩu mới'
        // ]);
        // $this->add($newPw);

        // $confirm_newPw = new Element\Password('confirm_newPw');
        // $confirm_newPw->setLabel('Nhập lại mật khẩu: ')->setLabelAttributes(['for'=>'confirm_newPw'])
        // ->setAttributes([
        //     'id'=>'confirm_newPw',
        //     'class'=>'form-control',
        //     'placeholder'=>'Nhập lại mật khẩu mới'
        // ]);
        // $this->add($confirm_newPw);

        //csrf
        $csrf = new \Zend\Form\Element\Csrf('csrf');
        $csrf->setAttributes([
            'id'=>'csrf',
            'class'=>'form-control',
        ])->setOptions([
            'csrf_options'=>[
                'timeout'=>600, //10 phut
            ],
        ]);
        $this->add($csrf);

         // Captcha image
        $this->add([
            'type'=>'captcha',
            'name'=>'captcha_image',
            'style'=>'display: block',
            'options'=>[
                'label'=>'Nhập chuỗi bên dưới: ',
                'captcha'=>[
                    'class'=>'Image',
                    'imgDir'=>'public/img/captcha', // Duong dan chinh xac luu duong dan
                    'imgUrl'=> RESOURCE_LINK.'/img/captcha/', // Duong dan hinh anh hien thi o browser
                    'suffix'=>'.png',
                    'font'=>APPLICATION_PATH.'/data/font/Anton-Regular.ttf',
                    'fsize'=>50,
                    'width'=>400,
                    'height'=>150,
                    'dotNoiseLevel'=>200,
                    'lineNoiseLevel'=>5,
                    'expiration'=>120, //120s,
                    'messages'=>[
                        \Zend\Captcha\Image::BAD_CAPTCHA => 'Giá trị biểu mẫu không đúng',
                    ]
                ]
            ]
        ]);

        //button Submit type=>Element\Submit::class
        $this->add([
            'name'=>'btnSubmit',
            'type'=>Element\Submit::class,
            'attributes'=>[
                'id'=>'btnSubmit',
                'class'=>'btn btn-success',
                'value'=>'Gửi'
            ],
        ]);

    }

    public function addValidator(){
        $inputfilter = new InputFilter();
        $this->setInputFilter($inputfilter);

        $inputfilter->add([
            'name'=>'email',
            'required'=>true,
            'filter'=>[
                'name'=>'StripTags',
                'name'=>'StripNewLines',
                'name'=>'StringTrim'
            ],
            'validators'=>[
                [
                    'name'=>'Regex',
                    'options'=>[
                        'pattern'=>"/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            Regex::INVALID=>'Lỗi cú pháp, vui lòng nhập đúng định dạng mail',
                        ]
                    ]
                ],
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Email không được bỏ trống'
                        ]
                    ]
                ],
                [
                    'name'=>'EmailAddress',
                    'break_chain_on_failure'=>true,
                    'messages'=>[
                        EmailAddress::INVALID=>'Định dạng mail không hợp lệ',
                        EmailAddress::INVALID_FORMAT=>'Không đúng cú pháp cho email',
                        EmailAddress::INVALID_HOSTNAME=>'Hostname không được hỗ trợ hoặc không đúng',
                        EmailAddress::INVALID_MX_RECORD=>'Lỗi MX',
                        EmailAddress::INVALID_SEGMENT=>'Lỗi Segment',
                        EmailAddress::DOT_ATOM=>'Vui lòng kiểm tra lại chuỗi hostname sau @',
                        EmailAddress::QUOTED_STRING=>'Chuỗi quoted không đúng',
                        EmailAddress::INVALID_LOCAL_PART=>'Địa chỉ part không hợp lệ',
                        EmailAddress::LENGTH_EXCEEDED=>'Độ dài cho email không hợp lệ',
                    ]
                ]
            ]
        ]);

         //new password
        //  $inputfilter->add([
        //     'name'=>'newPw',
        //     'required'=>true,
        //     'filter'=>[
        //         'name'=>'StringTrim',
        //         'name'=>'StripTags',
        //         'name'=>'StripNewLines'
        //     ],  
        //     'validators'=>[
        //         [
        //             'name'=>'NotEmpty',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'messages'=>[
        //                     NotEmpty::IS_EMPTY=>'Mật khẩu cũ không được bỏ trống'
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'StringLength',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'min'=>8,
        //                 'max'=>20,
        //                 'messages'=>[
        //                     StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
        //                     StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'Regex',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'pattern'=>'/[a-z]/',
        //                 'messages'=>[
        //                     \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
        //                     \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
        //                     \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'Regex',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'pattern'=>'/[A-Z]/',
        //                 'messages'=>[
        //                     \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
        //                     \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
        //                     \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'Regex',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'pattern'=>'/\d/',
        //                 'messages'=>[
        //                     \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
        //                     \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
        //                     \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
        //                 ]
        //             ]
        //         ],
        //     ]
        // ]);

        // //confirm new password
        // $inputfilter->add([
        //     'name'=>'confirm_newPw',
        //     'required'=>true,
        //     'filter'=>[
        //         'name'=>'StringTrim',
        //         'name'=>'StripTags',
        //         'name'=>'StripNewLines'
        //     ],  
        //     'validators'=>[
        //         [
        //             'name'=>'NotEmpty',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'messages'=>[
        //                     NotEmpty::IS_EMPTY=>'Mật khẩu cũ không được bỏ trống'
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'StringLength',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'min'=>8,
        //                 'max'=>20,
        //                 'messages'=>[
        //                     StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
        //                     StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'Regex',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'pattern'=>'/[a-z]/',
        //                 'messages'=>[
        //                     \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
        //                     \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
        //                     \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'Regex',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'pattern'=>'/[A-Z]/',
        //                 'messages'=>[
        //                     \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
        //                     \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
        //                     \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'Regex',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'pattern'=>'/\d/',
        //                 'messages'=>[
        //                     \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
        //                     \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
        //                     \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
        //                 ]
        //             ]
        //         ],
        //         [
        //             'name'=>'Identical',
        //             'options'=>[
        //                 'break_chain_on_failure'=>true,
        //                 'token'=>'newPw',
        //                 'messages'=>[
        //                     Identical::NOT_SAME=>'Password không giống nhau',
        //                     Identical::MISSING_TOKEN=>'Lỗi không xác định'
        //                 ]
        //             ],
        //         ],
        //     ]
        // ]);
    }
}





?>