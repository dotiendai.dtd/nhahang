<?php

namespace Users\Form;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Captcha;
use Zend\Form\Element;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;
use Zend\Validator\Regex;
use Zend\Validator\EmailAddress;
use Zend\Validator\Identical;
//use Zend\Captcha\ReCaptcha;

class UserForm extends Form{
    private $flag;
    public function __construct($flag = "add")
    {
        parent::__construct();
        $this->setAttributes([
            'name'=>'user-name',
            'class'=>'form-horizontal'
        ]);
        $this->flag = $flag;
        $this->addElement();    
        $this->Validator();
    }

    public function addElement(){

        //user name
        $username = new Element\Text('username');
        $username->setLabel('User name: ')->setLabelAttributes([
            'for'=>'username'
        ])->setAttributes([
            'class'=>'form-control',
            'id'=>'username',
            'placeholder'=>'Nhập user name'
        ]);
        $this->add($username);

        if($this->flag == "add"){
        //password
            $password = new Element\Password('password');
            $password->setLabel('Password: ')->setLabelAttributes([
                'for'=>'password'
            ])->setAttributes([
                'id'=>'password',
                'class'=>'form-control',
                'placeholder'=>'Nhập password'
            ]);
            $this->add($password);

            //confirm password
            $confirmPassword = new Element\Password('confirmPassword');
            $confirmPassword->setLabel('Confirm Password: ')->setLabelAttributes([
                'for'=>'confirmPassword'
            ])->setAttributes([
                'id'=>'confirmPassword',
                'class'=>'form-control',
                'placeholder'=>'Nhập confirm password'
            ]);
            $this->add($confirmPassword);
        }

        //Email
        $email = new Element\Email('email');
        $email->setLabel('Email: ')->setLabelAttributes([
            'for'=>'email'
        ])->setAttributes([
            'id'=>'email',
            'class'=>'form-control',
            'placeholder'=>'Nhập email'
        ]);
        $this->add($email);

        //full name
        $fullname = new Element('fullname');
        $fullname->setLabel('Fullname: ');
        $fullname->setLabelAttributes([
            'for'=>'fullname',
            'class'=>'control-label'
        ]);
        $fullname->setAttributes([
            'id'=>'fullname',
            'type'=>'text',
            'class'=>'form-control',
            'placeholder'=>'Nhap ho va ten: '
        ]);
        $this->add($fullname);

         //birthday
         $this->add([
            'name'=>'birthday',
            'type'=>Element\Date::class,
            'attributes'=>[
                'class'=>'form-control',
                'id'=>'birthday'
            ],
            'options'=>[
                'label'=>'Choose Birthday Date: ',
                'class'=>'label-control',
               // 'format'=>'d-m-Y',
            ]
        ]);

         //Gender
         $gender = new Element\Select('gender');
         $gender->setLabel('Gender: ')->setLabelAttributes([
             'class'=>'lable-control'
         ]);
         $gender->setAttributes([
             'id'=>'gender',
             'class'=>'form-control',
             'value'=>'male'
         ]);
         $gender->setValueOptions([
             'male'=>' Nam',
             'female'=>' Nữ',
             'other'=>' Khác',
         ]);
         $this->add($gender);

         //Address
         $address = new Element\Text('address');
         $address->setLabel('Address: ')->setLabelAttributes([
             'for'=>'address'
         ])->setAttributes([
             'id'=>'address',
             'class'=>'form-control',
             'placeholder'=>'Nhập address'
         ]);
         $this->add($address);

         //Phone
         $phone = new Element\Text('phone');
         $phone->setLabel('Phone: ')->setLabelAttributes([
             'for'=>'phone'
         ])->setAttributes([
             'id'=>'phone',
             'class'=>'form-control',
             'placeholder'=>'Nhập phone'
         ]);
         $this->add($phone);

         //Role 
         $role = new Element\Select('role');
         $role->setLabel('Role: ')->setLabelAttributes([
             'class'=>'lable-control'
         ]);
         $role->setAttributes([
             'id'=>'role',
             'class'=>'form-control',
             'value'=>'male'
         ]);
         $role->setValueOptions([
             'admin'=>' Quản trị viên',
             'customer'=>' Khách hàng',
             'Staff'=>' Nhân viên',
         ]);
         $this->add($role);

           //Captcha image
        // $this->add([
        //     'type'=>'captcha',
        //     'name'=>'g-recaptcha-response',
        //     'options'=>[
        //         'label'=>'Enter String Below: ',
        //         'captcha'=>[
        //             'id'=>'captcha',
        //             'class'=>'Image',
        //             'imgDir'=>'public/img/captcha', // Duong dan chinh xac luu duong dan
        //             'imgUrl'=> RESOURCE_LINK.'/img/captcha/', // Duong dan hinh anh hien thi o browser
        //             'suffix'=>'.png',
        //             'font'=>APPLICATION_PATH.'/data/font/PressStart2P-Regular.ttf',
        //             'fsize'=>50,
        //             'width'=>400,
        //             'height'=>150,
        //             'dotNoiseLevel'=>200,
        //             'lineNoiseLevel'=>5,
        //             'expiration'=>120, //120s,
        //             'messages'=>[
        //                 \Zend\Captcha\Image::BAD_CAPTCHA => 'Ok fine',
        //             ]
        //         ]
        //     ]
        // ]);

        //button Submit type=>Element\Submit::class
        $this->add([
            'name'=>'btnSubmit',
            'type'=>Element\Submit::class,
            'attributes'=>[
                'id'=>'btnSubmit',
                'class'=>'btn btn-success',
                'value'=>'Save'
            ],
        ]);
    }

    public function Validator(){
        $inputfilter = new InputFilter();
        $this->setInputFilter($inputfilter);

        //username filter
        $inputfilter->add([
            'name'=>'username',
            'required'=>true,
            'filter'=>[
                ['name'=>'StringTrim'],
                ['name'=>'StringToLower'],
                ['name'=>'StripTags'],
                ['name'=>'StripNewLines']
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Username Không được rỗng'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>6,
                        'max'=>18,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Username quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Username quá dài, tối đa %max% kí tự',
                        ]
                    ],
                ],
            ],
        ]);

     
        if($this->flag == "add"){
               //password filter
        $inputfilter->add([
            'name'=>'password',
            'required'=>true,
            'filter'=>[
                ['name'=>'StringTrim'],
                ['name'=>'StripTags'],
                ['name'=>'StripNewLines']
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Password không được rỗng'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>8,
                        'max'=>20,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
                        ]
                    ],
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[a-z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/[A-Z]/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/\d/',
                        'messages'=>[
                            \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                            \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
                            \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                        ]
                    ]
                ],
            ],
        ]);

        //confirm password filter
        $inputfilter->add([
            'name'=>'confirmPassword',
            'required'=>true,
            'filter'=>[
                ['name'=>'StringTrim'],
                ['name'=>'StripTags'],
                ['name'=>'StripNewLines']
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Confirm password không được rỗng'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>8,
                        'max'=>20,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự',
                        ]
                    ],
                ],
                [
                    'name'=>'Identical',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'token'=>'password',
                        'messages'=>[
                            Identical::NOT_SAME=>'Password không giống nhau',
                            Identical::MISSING_TOKEN=>'Lỗi không xác định'
                        ]
                    ],
                ],
            ],
        ]);

        }
        //fullname filter
        $inputfilter->add([
            'name'=>'fullname',
            'required'=>true,
            'filter'=>[
                ['name'=>'StringTrim'],
                ['name'=>'StripTags'],
                ['name'=>'StripNewLines']
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Full name không được rỗng'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'max'=>80,
                        'messages'=>[
                            StringLength::TOO_LONG=>'User name quá dài, tối đa %max% kí tự',
                        ]
                    ],
                ],
            ],
        ]);

        //birthday filter
        $inputfilter->add([
            'name'=>'birthday',
            'required'=>true,
            'validators'=>[
                [
                    'name'=>'Date',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            \Zend\Validator\Date::INVALID_DATE=>'Nhập đúng định dạng ngày tháng',
                        ]
                    ]
                ],
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Ngày sinh không được rỗng'
                        ]
                    ]
                ],
            ],
        ]);

        //email filter
        $inputfilter->add([
            'name'=>'email',
            'required'=>true,
            'filter'=>[
                ['name'=>'StringTrim'],
                ['name'=>'StringToLower'],
                ['name'=>'StripTags'],
                ['name'=>'StripNewLines']
            ],
            'validators'=>[
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>"/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                        'messages'=>[
                            Regex::INVALID=>'Email cần chứa %pattern%'
                        ]
                    ]
                ],
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Email không được rỗng'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>10,
                        'max'=>50,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Email quá ngắn, ít nhất %min% kí tự',
                            StringLength::TOO_LONG=>'Email quá dài, tối đa %max% kí tự'
                        ]
                    ],
                ],
                [
                    'name'=>'EmailAddress',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            EmailAddress::INVALID=>'Không đúng định dạng', //còn mấy cái như @hostname,...
                        ]
                    ]
                ],
            ],
        ]);

        //phone filter
        $inputfilter->add([
            'name'=>'phone',
            'required'=>true,
            'filter'=>[
                ['name'=>'StringTrim'],
                ['name'=>'StripTags'],
                ['name'=>'StripNewLines']
            ],
            'validators'=>[
                [
                    'name'=>'NotEmpty',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'messages'=>[
                            NotEmpty::IS_EMPTY=>'Phone không được rỗng'
                        ]
                    ]
                ],
                [
                    'name'=>'StringLength',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'min'=>7,
                        'max'=>11,
                        'messages'=>[
                            StringLength::TOO_SHORT=>'Số điện thoại quá ngắn',
                            StringLength::TOO_LONG=>'Số điện thoại quá dài'
                        ]
                    ],
                ],
                [
                    'name'=>'Regex',
                    'options'=>[
                        'break_chain_on_failure'=>true,
                        'pattern'=>'/\d/',
                        'messages'=>[
                            Regex::INVALID=>'Phone không đúng định dạng'
                        ]
                    ]
                ]
            ],
        ]);
    }
}
?>