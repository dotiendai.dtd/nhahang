<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Users;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Controller\AbstractActionController;
use Users\Service\AuthManager;
use Users\Controller\AuthController;


class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    public function getAutoloaderConfig(){ // dinh nghia duong dan namespace
        return [
            'Zend\Loader\StandardAutoLoader'=>[
                'namespace'=>[
                    __NAMESPACE__=>__DIR__.'/src/'.__NAMESPACE__,
                ]
            ]
        ];
    }

    public function onBootstrap(MvcEvent $event){
        $eventManager = $event->getApplication()->getEventManager();
        $shareEventManager = $eventManager->getSharedManager();
        $shareEventManager->attach(AbstractActionController::class, MvcEvent::EVENT_DISPATCH,[$this,'onDispath'],100);
    
    }

    public function onDispath(MvcEvent $event){
        $controllerName = $event->getRouteMatch()->getParam('controller',null);
        $actionName = $event->getRouteMatch()->getParam('action',null);

        $authManager = $event->getApplication()->getServiceManager()->get(AuthManager::class);
        if(!$authManager->filterAccess($controllerName, $actionName) && $controllerName != AuthController::class){

            // Khoong co quyen
            $controller = $event->getTarget();
            return $controller->redirect()->toRoute('auth',['action'=>'login']);
        }
        
    }
}
