<?php

//__construct($entityManager) // dang lam theo Doctrine
//authenticate()
namespace Users\Service;

use Zend\Authentication\Adapter\AdapterInterface;
use Users\Entity\Users;
use Zend\Authentication\Result;
use Zend\Crypt\Password\Bcrypt;

class AuthAdapter implements AdapterInterface{ //chuc nang khai bao cot ma minh se cho ng dung dang nhap,
    // kiem tra vs db xem ng dung co ton tai hay k
    private $entityManager;
    private $username;
    private $password;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function setUsername($username){
        $this->username = $username;
    }
    public function setPassword($password){
        $this->password = $password;
    }
    public function authenticate(){
        $user = $this->entityManager->getRepository(Users::class)
        ->findOneByUsername($this->username);
        if(!$user){
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null,['Tên đăng nhập không hợp lệ.']);
        }else{
            $bcrypt = new Bcrypt();
            $userPassword = $this->password;
            $dbPassword = $user->getPassword();
            if($bcrypt->verify($userPassword, $dbPassword)){
                return new Result(Result::SUCCESS, $this->username,['Đăng nhập thành công.']);
            }else{
                return new Result(Result::FAILURE_CREDENTIAL_INVALID,null,['Sai mật khẩu']);
            }
        }
    }

}