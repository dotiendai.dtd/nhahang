<?php
//dung de thuc thi login logout
namespace Users\Service;

// use Zend\Db\Adapter\Driver\Sqlsrv\Result;
use Users\Service\AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;

class AuthManager{
    private $authenticationService;
    private $sessionManager;
    private $config;

    public function __construct($sessionManager, $authenticationService, $config)
    {
        $this->sessionManager = $sessionManager;
        $this->authenticationService = $authenticationService;
        $this->config = $config;
    }
    public function login($username, $password, $rememberMe){
        if($this->authenticationService->hasIdentity()){
            throw new \Exception('Ban da dang nhap');
        }
        $authAdapter = $this->authenticationService->getAdapter();
        $authAdapter->setUsername($username);
        $authAdapter->setPassword($password);

        $result = $this->authenticationService->authenticate();
        if($result->getCode() == Result::SUCCESS && $rememberMe){
            $this->sessionManager->rememberMe(86400*30); //30 ngay
        }
        return $result;
    }

    public function logout(){
        if($this->authenticationService->hasIdentity()){
            $this->authenticationService->clearIdentity();
        }else{
            throw new \Exception('Ban chua dang nhap');
        }
    }

    public function filterAccess($controllerName, $actionName){
        if(isset($this->config['controllers'][$controllerName])){
            $controllers = $this->config['controllers'][$controllerName];
            //print_r($controllers);
            foreach($controllers as $controller){
                $listAction = $controller['actions'];
                $allow = $controller['allow'];
                if(in_array($actionName, $listAction)){
                    if($allow=="all"){
                        return true;
                    }elseif ($allow == "limit" && $this->authenticationService->hasIdentity()) {
                        # code...
                        return true;
                    }else return false;
                }
            }
        }
        else{
            if(!$this->authenticationService->hasIdentity()){
                return false;
            }
        }
        return true;
    }
}