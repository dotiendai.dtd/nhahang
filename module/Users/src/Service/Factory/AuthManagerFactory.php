<?php

namespace Users\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Session\SessionManager;
use Zend\Authentication\AuthenticationService;
use Users\Service\AuthManager;

class AuthManagerFactory implements FactoryInterface{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $sessionManager = $container->get(SessionManager::class);
        $authenticationService = $container->get(AuthenticationService::class);

        $config = $container->get('Config');
        if(isset($config['access_filter'])){
            $config = $config['access_filter'];
        }else{
            $config = [];
        }
        return new AuthManager($sessionManager, $authenticationService, $config);
    }
}