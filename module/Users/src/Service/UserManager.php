<?php

namespace Users\Service;
use Users\Entity\Users;
use PHPUnit\Runner\Exception;
use Zend\Crypt\Password\Bcrypt;
use Zend\Mail\Message;
use Zend\Mail\Transport;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Math\Rand;
use Zend\I18n\Validator\DateTime;

class UserManager{
    private $entityManager;
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function checkEmailExists($email){
        $user = $this->entityManager->getRepository(Users::class)->findOneByEmail($email);
        // if($user!=null) return true;
        // return false;
        return $user != null;

    }
    public function checkUsernameExists($username){
        $user = $this->entityManager->getRepository(Users::class)->findOneByUsername($username);
        return $user != null;
    }
    public function addUser($data){
        if($this->checkEmailExists($data['email'])){
            throw new \Exception("Email ".$data['email']." đã có người sử dụng");
        }
        if($this->checkUsernameExists($data)){
            throw new Exception("User đã có người sử dụng.");
        }

    //`id`, `username`, `password`, `fullname`, `birthdate`, `gender`, 
    //`address`, `email`, `phone`, `role`, `pw_reset_token`, `pw_reset_token_date`
        $user = new Users();
        $user->setUsername($data['username']);
        $user->setFullname($data['fullname']);

        $birthdate = new \DateTime($data['birthday']);
        $birthdate->format('Y-m-d');
        $user->setBirthdate($birthdate);

       // $user->setBirthdate($data['birthday']);
        $user->setGender($data['gender']);
        $user->setAddress($data['address']);
        $user->setEmail($data['email']);
        $user->setPhone($data['phone']);
        $user->setRole($data['role']);

        $bcrypt = new Bcrypt();
        $securePass = $bcrypt->create($data['password']);
        $user->setPassword($securePass);

        $this->entityManager->persist($user); //set data tuong ung vs cac cot (tao ra 1 user moi)
        $this->entityManager->flush(); // save data vao db
        return $user;

    }

    public function editUser($user, $data){
        // if($this->checkEmailExists($data['email'])){
        //     throw new \Exception("Email ".$data['email']." đã có người sử dụng");
        // }
        $sql = "Select u from Users\Entity\Users u where u.email = '".$data['email']."' and u.username != '".$data['username']."'";
        $q = $this->entityManager->createQuery($sql);
        $users = $q->getResult();
        if(!empty($users)){
            throw new \Exception("Email ".$data['email']." đã có người sử dụng");
        }

        $user->setFullname($data['fullname']);
        $birthdate = new \DateTime($data['birthday']);
        $birthdate->format('Y-m-d');
        $user->setBirthdate($birthdate);
        $user->setGender($data['gender']);
        $user->setAddress($data['address']);
        $user->setEmail($data['email']);
        $user->setPhone($data['phone']);
        $user->setRole($data['role']);

        $this->entityManager->flush(); // save data vao db
        return $user;
    }

    public function deleteUser($user){
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
    public function verifyPassword($password, $securePass){
        $bcrypt = New Bcrypt();
        if ($bcrypt->verify($password, $securePass)) {
           return true;
        }
        return false;
    }
    public function changeNewPassword($user, $data){
        $securePass = $user->getPassword(); //mk trong db //đã mã hóa
        $password = $data['oldPwd']; // Nguoi dung nhap // chưa mã hóa
        // echo $password;
        // echo "<br>";
        // echo $securePass; die;
        if(!$this->verifyPassword($password, $securePass)){
            return false;
        }
        $newPw = $data['newPwd'];
        $bcrypt = new Bcrypt();
        $securePass = $bcrypt->create($newPw);
        $user->setPassword($securePass);
        $this->entityManager->flush();
        return true;
    }

    public function createTokenToResetPw($user){ // lưu vào db
        $token = Rand::getString(32,"0123456789qwertyuiopasdfghjklzxcvbnm",true); //Độ dài của chuỗi và các chữ cái
        $user->setPw_reset_token($token);

        $dateCreate = date('Y-m-d H:i:s');
        $dateCreate = new \DateTime($dateCreate);
        $dateCreate->format('Y-m-d H:i:s');
        $user->setPw_reset_token_date($dateCreate);
        $this->entityManager->flush();

        $http = isset($_SERVER['HTTPS']) ? "https://" : "http://";
        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : "localhost:8888";
        $url = $http.$host."/nhahang/public/setpassword/".$token;
        //print_r($url); die;
        $bodyMessage = "Chào bạn, ".$user->getFullname()." 
                        Bạn vui lòng nhập vào link bên dưới để reset password: 
                        $url 
                        Nếu bạn không yêu cầu đặt lại mật khẩu, vui lòng bỏ qua tin nhắn này.";
        $message = new Message();
        $message->addTo($user->getEmail());
        $message->addFrom('loveabletruog@gmail.com');
        $message->setSubject('Reset Password');
        $message->setBody($bodyMessage);
        
        // Setup SMTP transport using LOGIN authentication
        $transport = new SmtpTransport();
        $options   = new SmtpOptions([
            'name'              => 'smtp.gmail.com',
            'host'              => 'smtp.gmail.com',
            'port'              =>465,   
            'connection_class'  => 'login',
            'connection_config' => [
                'username' => 'loveabletruog@gmail.com',
                'password' => 'dieuquynh8',
                'ssl'      =>'ssl'
            ],
        ]);
        $transport->setOptions($options);
        $transport->send($message);     
          
    }

    public function checkResetPasswordToken($token){
            $user = $this->entityManager->getRepository(Users::class)
            ->findOneBy(['pw_reset_token'=>$token]);
         //   var_dump($user);
            if(!$user){
                return false;
            }
            $userTokenDate = $user->getPw_reset_token_date()->getTimestamp();
        //  var_dump($userTokenDate); die;
            $now = new \Datetime('now');
            //var_dump($now); die;
            $now = $now->getTimestamp();
          //  var_dump($now); die;
            if(($now - $userTokenDate) > 86400){ //24*60*60 // =  1 day
                return false;
            }
            return true;
    }

    public function setNewPasswordByToken($token, $newPassword){
        if(!$this->checkResetPasswordToken($token)){
            return false;
        }
        $user = $this->entityManager->getRepository(Users::class)
            ->findOneBy(['pw_reset_token'=>$token]);
        if(!$user){
            return false;
        }
        $brypt = new Bcrypt();
        $passHash = $brypt->create($newPassword);
        $user->setPassword($passHash);
        //reset pw token
        $user->setPw_reset_token(null);
        $user->setPw_reset_token_date(null);
        $this->entityManager->flush();
        return true;
    }

}